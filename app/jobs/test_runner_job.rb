class TestRunnerJob < ApplicationJob
  include RunsHelper, ProjectsHelper
  queue_as :default

  def perform(runid, projectid)
    project = Project.find(id: projectid)
    run = project.runs.find(id: runid)
    run.run_no = next_run_no(project)
    run.save
    begin
      schedule_run(run)
    rescue StandardError => err
      logger.info err.message
      logger.info err.backtrace
      puts err.message
      puts err.backtrace
      run.status = 'error'
      run.notes = err.message
      run.save
    ensure
      #Reload latest run data
      project = Project.find(id: projectid)
      run = project.runs.find(id: runid)
      update_status(run, run.status)
    end
  end
end
