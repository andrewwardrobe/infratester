class ProjectImporterJob < ApplicationJob
  include ProjectsHelper
  queue_as :default

  def perform(projectid)
    project = Project.find(id: projectid)
    puts "project #{project}"
    begin
      import_properties(project)
    rescue StandardError => e
      logger.error e.message
      logger.error e.backtrace.join("\n")
      raise e
    end
  end
end
