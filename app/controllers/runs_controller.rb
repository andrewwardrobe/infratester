class RunsController < ApplicationController
  include RunsHelper
  before_action :set_run, only: [:show, :edit, :update, :destroy, :report]
  before_action :set_project, except: [:update, :show, :edit]

  # GET /runs
  # GET /runs.json
  def index
    @project = Project.find(params[:project_id])
    @runs = @project.runs
  end

  # GET /runs/1
  # GET /runs/1.json
  def show
  end

  # GET /runs/new
  def new
    @run = Run.new(project: @project)
  end

  # GET /runs/1/edit
  def edit
  end


  # POST /runs
  # POST /runs.json
  def create
    @run = Run.new(run_params.merge(project: @project))

    respond_to do |format|
      if @run.save
        TestRunnerJob.perform_later(@run.id.to_s, @project.id.to_s)
        format.html { redirect_to @run.project, notice: 'Run was successfully scheduled.' }
        format.json { render :show, status: :created, location: @run }
      else
        format.html { render :new }
        format.json { render json: @run.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /runs/1
  # PATCH/PUT /runs/1.json
  def update
    respond_to do |format|
      if @run.update(run_params)
        data = render partial: 'runs/run', locals: {run: @run}
        ActionCable.server.broadcast("runs_#{@run.project.id}", id: @run.id.to_s, body: data)
        format.html { redirect_to @run, notice: 'Run was successfully updated.' }
        format.json { render :show, status: :ok, location: @run }
      else
        format.html { render :edit }
        format.json { render json: @run.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /runs/1
  # DELETE /runs/1.json
  def destroy
    @run.destroy
    respond_to do |format|
      format.html { redirect_to @run.project, notice: 'Run was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /runs/1/report/server1.json
  # GET /runs/1/report/server1.json
  def report
    @suite = params[:suite]
    respond_to do |format|
      format.html { render :report }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_run
    @project = Project.find(params[:project_id])
    @run = @project.runs.find(params[:id])
  end

  def set_project
    @project = Project.find(params[:project_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def run_params
    params.require(:run).permit(:status, :result, :notes, :targets, :branch, :test_report => {}, :summary => {})
  end


end
