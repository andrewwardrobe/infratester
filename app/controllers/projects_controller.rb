class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :environments,
                                     :destroy, :refresh, :targets]

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.all
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @runs = @project.runs
    params[:project_id] = @project.id
  end


  def targets
    @targets = ['all'] << @project.settings.where(branch: params[:branch]).first[:targets].map {|t| t[0]}
    @targets.flatten!
    respond_to do |format|
      format.json { render :targets, status: :ok, location: @project }
    end
  end

  def environments
    env = @project.settings.where(branch: params[:branch]).first[:environments] || {}
    @environments = ['all'] << env.map {|t| t[0]}
    @environments.flatten!
    respond_to do |format|
      format.json { render :environments, status: :ok, location: @project }
    end
  end
  # POST /projects/refresh
  # POST /projects/refresh.json
  def refresh
    ProjectImporterJob.perform_later(params[:id])
    respond_to do |format|
      format.html { redirect_to @project, notice: 'Updating Project' }
      format.json { render :show, status: :ok, location: @project }
    end
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)
    logger.info @project.to_yaml
    respond_to do |format|
      if @project.save
        logger.info @project.to_yaml
        id = @project.id.to_s
        ProjectImporterJob.perform_later(id)
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :repo_url)
    end
end
