# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_layout
  before_action :application_name


  def application_name
    @application_name = 'Serverspec Launcher'
  end

  private def set_layout
    @layout = 'default'
  end


end
