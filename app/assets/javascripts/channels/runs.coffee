$(document).ready ->
  if $('#pageType').val() == 'project_show'
    App.runs = App.cable.subscriptions.create(channel: "RunsChannel", id: $('#projectId').val(),
      received: (data) ->
        $('#run_' + data.id).html data.body

      connected: ->
        console.log "Connected"

      disconnected: ->
    )