$(document).ready ->
  if $('#pageType').val() == 'project_show'
    App.settings = App.cable.subscriptions.create(channel: 'SettingsChannel', id: $('#projectId').val(),
        received: (data) ->
          $('#settingsDiv').html data.body

        connected: ->
          console.log "Connected"
    )
