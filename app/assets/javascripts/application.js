// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require popper
//= require jquery3
//= require jquery.turbolinks
//= require jquery_ujs
//= require tether
//= require bootstrap
//= require_tree .

jQuery(document).ready(function($) {
    $('*[data-href]').on('click', function() {
        window.location = $(this).data("href");
    });
    $('#branchSelect').on('change', function(e) {
        $('.tab-pane').removeClass('active show')
        $('#' + $(e.currentTarget).val()).addClass("active show");
        $('#run_branch').val($(e.currentTarget).val());
    })
    $('#' + $('#branchSelect').val()).addClass("active show");
    $('.collapse-button').click(function(){ //you can give id or class name here for $('button')
        $(this).text(function(i,old){
            return old=='+' ?  '-' : '+';
        });
    });
});

