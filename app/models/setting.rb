class Setting
  include Mongoid::Document
  field :branch, type: String
  field :targets, type: Hash
  field :environments, type: Hash
  field :options, type: Hash
  field :shared_example_gems, type: Array
  field :environment, type: Hash
  field :variables, type: Hash
end
