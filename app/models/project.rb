class Project
  include Mongoid::Document
  embeds_many :runs
  embeds_many :settings
  field :name, type: String
  field :repo_url, type: String
  field :next_run_no, type: Integer, default: 1

  before_validation :strip_whitespace

  private
  def strip_whitespace
    self.repo_url = repo_url.strip unless repo_url.nil?
  end
end
