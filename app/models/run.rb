class Run
  include Mongoid::Document
  embedded_in :project

  field :status, type: String, default: 'pending'
  field :result, type: String
  field :notes, type: String
  field :targets, type: String, default: 'all'
  field :test_report, type: Hash, default: {}
  field :last_update, type: DateTime
  field :run_no, type: Integer
  field :summary, type: Hash
  field :branch, type: String

end
