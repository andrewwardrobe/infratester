# frozen_string_literal: true

module ApplicationHelper
  def render_haml(haml, locals = {})
    Haml::Engine.new(haml.strip_heredoc, format: :html5).render(self, locals)
  end
  def bootstrap_class_for(flash_type)
    case flash_type
    when 'success'
      'alert-success'
    when 'error'
      'alert-error'
    when 'alert'
      'alert-block'
    when 'notice'
      'alert-info'
    else
      flash_type.to_s
    end
  end

  def test_result_css(status)
    if status == 'passed'
      'test-passed'
    elsif status == 'pending'
      'test-pending'
    else
      'test-failed'
    end
  end

  def indentation_css(level)
    "indent-#{level}"
  end

  def get_style(style)
    case style
    when :light, :fal
      'fal'
    when :brand, :fab
      'fab'
    when :regular, :far
      'far'
    when :solid, :fas
      'fas'
    else
      'fas'
    end
  end

  def fa_link_to(icon_name, link, text = nil, **options)
    style = options.dig(:type)
    icon = content_tag(:i, nil, :class => "#{get_style(style)} fa-#{icon_name}")
    link_to icon_join(icon, text), link, **options
  end


  # From https://github.com/bokmann/font-awesome-rails/blob/master/app/helpers/font_awesome/rails/icon_helper.rb
  def icon_join(icon, text, reverse_order = false)
    return icon if text.blank?
    elements = [icon, ERB::Util.html_escape(text)]
    elements.reverse! if reverse_order
    safe_join(elements, " ")
  end

  def indent(level)
    ' ' * (level * 2)
  end

  def test_result_icon(status)
    if status == 'passed'
      "\u2713"
    elsif status == 'pending'
      "\u2729"
    else
      "\u2717"
    end
  end

  def button_css(status)
    if status == 'passed'
      "btn-success"
    elsif status == 'pending'
      "btn-warning"
    else
      "btn-danger"
    end
  end

  def render_yaml(data, tree = [], level = 0, collaspe = false)
    text = render_data(data, tree, level, collaspe)
    render_haml text
  end

  def render_text(data, tree, level = 0)
    text = "#{indent(level)}%span.yaml-text #{data}\n"
    text
  end

  def get_tree(tree, item)
    tree.join('_') + "_#{item}"
  end

  def render_hash(data, tree = [], level = 0, collapse)
    css_class = "#{indentation_css(level)}"
    text = "#{indent(level)}%div.#{css_class}\n"
    data.each do |key, val|
      tree_data = get_tree(tree, key)
      text += "#{indent(level + 1)}%div.yaml-hash\n"
      text += "#{indent(level + 2)}%span.yaml-key #{key}:\n"

      text += render_collapsible(level + 2, tree, tree_data, val, collapse)
    end
    text
  end

  def render_array(data, tree = [], level = 0, collaspe)
    css_class = "#{indentation_css(level)}"
    text = ""
    data.each do |item|
      text += "#{indent(level)}%div.yaml-array.#{css_class}\n"
      text += "#{indent(level + 1)}\\- \n"
      text += "#{render_data(item, tree, level + 1, collaspe)}\n"
    end
    text
  end

  private

  def render_collapsible(level, tree, tree_data, val, collapse)
    text = ""
    indent_lvl = if val.is_a?(Hash) || val.is_a?(Array)
                   text += "#{indent(level)}%span\n"
                   text += "#{indent(level + 1)}%a{'data-toggle': 'collapse', 'aria-expanded': false, role: 'button', " +
                       "'aria-controls': '#{tree_data}', href: '\##{tree_data}', " +
                       "class: 'collapse-button'} -\n"
                   text += "#{indent(level)}%div{id: '#{tree_data}', class: '#{collapse_css(collapse)}'}\n"

                   1
                 else
                   0
                 end
    text += render_data(val, tree, level + indent_lvl, collapse)
    text
  end

  def collapse_css(collapse = false)
    "collapse #{collapse ? '' : 'show'}"
  end


  def render_data(val, tree = [], level = 0, collaspe)
    if val.is_a?(Array)
      render_array val, tree, level, collaspe
    elsif val.is_a? Hash
      render_hash val, tree, level, collaspe
    elsif val.is_a? String
      render_text val, tree, level
    else
      render_text val.to_s, tree, level
    end
  end

  def button_style
    'btn btn-black border border-white'
  end

  def input_style
    'form-control border border-white bg-dark text-white'
  end

end
