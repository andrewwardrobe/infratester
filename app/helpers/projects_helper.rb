module ProjectsHelper

  def import_properties(project)
    settings = settings_from_git(project)
    broadcast_update(project, settings)
  end

  def broadcast_update(project, settings)
    data = ApplicationController.render partial: 'projects/settings', locals: {project: project, active_branch: 'master'}
    ActionCable.server.broadcast("settings_#{project.id}", body: data)
  end



  def next_run_no(project)
    run_id = project.next_run_no
    project.next_run_no = run_id + 1
    project.save
    run_id
  end

  private

  def get_branches(branches)
    branches.select{|b| b.remote }.reject {|b| b.name.include?('HEAD') }
  end

  def settings_from_git(project)
    FileUtils.rm_rf("/tmp/#{project.name}") if Dir.exists? "/tmp/#{project.name}"
    settings = []
    begin
      Dir.chdir('/tmp/') do
        g = Git.clone(project.repo_url, project.name)
        get_branches(g.branches).each do |branch|
          g.branch(branch.full).checkout
          data = YAML.load(File.open("#{project.name}/properties.yml")).symbolize_keys
          setting = Setting.new
          setting[:branch] = branch.name
          setting[:targets] = data[:targets] || {}
          setting[:environments] = data[:environments] || {}
          setting[:options] = data[:options] || {}
          setting[:shared_example_gems] = data[:shared_example_gems] || []
          setting[:environment] = data[:environment] || {}
          setting[:variables] = data[:variables] || {}
          settings << setting
        end
        project.settings = settings
        project.save
      end
    rescue StandardError => e
      logger.info e.message
      logger.info e.backtrace.join("\n")
      raise e
    ensure
      FileUtils.rm_rf("/tmp/#{project.name}") if Dir.exists? "/tmp/#{project.name}"
    end
    settings
  end
end
