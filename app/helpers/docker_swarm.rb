require 'docker'
require 'docker-swarm-sdk'


class DockerSwarm
  def initialize
    @master_connection = Docker::Swarm::Connection.new(ENV['DOCKER_HOST'] || 'unix:///var/run/docker.sock', load_certs)
    @swarm = Docker::Swarm::Swarm.swarm(@master_connection)
  end

  def load_certs
    if ENV['DOCKER_CERT_PATH']
      cert_path = ENV['DOCKER_CERT_PATH']
      cert_store = OpenSSL::X509::Store.new
      certificate = OpenSSL::X509::Certificate.new File.read(File.join(cert_path, 'ca.pem'))
      cert_store.add_cert certificate
      {
          client_cert_data: File.read(File.join(cert_path, 'cert.pem')),
          client_key_data: File.read(File.join(cert_path, 'key.pem')),
          ssl_cert_store: cert_store,
          scheme: 'https'
      }
    else
      {}
    end
  end

  def create_service(name:, image:, **options)
    @swarm.create_service service_options(name: name, image: image, opts: options)
  end

  def delete_service(name)
    service = @swarm.find_service_by_name(name)
    service&.remove
  end

  def list_tasks
    @swarm.tasks
  end

  def task_finished(service)
    %w(complete failed).include? check_service_status(service)
  end

  def check_service_status(service)
    tasks = @swarm.tasks.find {|task| task.hash['ServiceID'] == service.id }.hash
    tasks['Status']['State']
  end

  def create_secret(name:, data:, filename: name, labels: {})
    body = { "Name": name, "Data": data, "Labels": labels }
    resp = @master_connection.post('/secrets/create', {}, body: body.to_json, full_response: true, expects: [201, 409, 500, 503])
    { "SecretID": JSON.parse(resp[:body])['ID'], "SecretName": name, "File": { Name: filename } }
  end

  def list_configs
    resp = @master_connection.get('/configs', full_response: true, expects: [201, 409, 500, 503])
    JSON.parse(resp.body)
  end

  def config_exists?(config_name)
    list_configs.find {|config| config.dig('Spec','Name') == config_name }
  end

  def create_config(name:, data:, filename: name, labels: {})
    body = { "Name": name, "Data": data, "Labels": labels }
    resp = @master_connection.post('/configs/create', {}, body: body.to_json, full_response: true, expects: [201, 409, 500, 503])
    { "ConfigID": JSON.parse(resp[:body])['ID'], "ConfigName": name, "File": { Name: filename } }
  end
  private

  # rubocop:disable Metrics/MethodLength
  def default_service_options
    {
        mounts: [],
        restart_policy: 'any',
        restart_limit: 3,
        restart_delay: 5,
        environment: [],
        user: 'root',
        ports: [],
        placement_constraints: [],
        hosts: [],
        networks: [],
        labels: {},
        global: false,
        replicas: 1,
        secrets: [],
        configs: []
    }
  end
  # rubocop:enable Metrics/MethodLength

  # rubocop:disable Metrics/MethodLength
  def service_options(name:, image:, opts: {})
    options = default_service_options.merge(opts)
    {
        "Name": name,
        "TaskTemplate": {
            "ContainerSpec": { "Networks": [], "Image": image, "Mounts": options[:mounts], "Env": options[:environment], "User": options[:user],
                               "Hosts": options[:hosts], "Secrets": options[:secrets], "Configs": options[:configs] }.reject { |_k, v| v.nil? },

            "LogDriver": { " Name": 'json-file', "Options": { "max-file": '3', "max-size": '10M' } },
            "Placement": { "Constraints": options[:placement_constraints] },
            "RestartPolicy": { "Condition": options[:restart_policy], "Delay": options[:restart_delay],
                               "MaxAttempts": options[:restart_limit] },
            "Networks": options[:networks]
        },
        "Mode": (options[:global] ? { "Global": {} } : {
            "Replicated": { "Replicas": options[:replicas] }
        }),
        "EndpointSpec": {
            "Ports": options[:ports]
        },
        "Labels": options[:labels]
    }
  end
  # rubocop:enable Metrics/MethodLength
end
