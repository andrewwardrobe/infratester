
module RunsHelper

  def schedule_run(run)
    update_status(run, 'scheduling')
    swarm = DockerSwarm.new
    service = create_service(run, swarm)
    until swarm.task_finished(service) do
      update_status(run, swarm.check_service_status(service))
      sleep(0.2)
    end
    update_status(run, swarm.check_service_status(service))
    swarm.delete_service(service.name)
  end

  private

  def create_service(run, swarm)
    swarm.create_service name: "serverspec_#{Time.now.to_i}",
                         image: 'manager1:5000/andrewwardrobe/infratester-runner:latest',
                         restart_policy: 'none',
                         networks: [{"Target": 'infratester_launcher'}],
                         environment: %W[
                                       RUN_ID=#{run.id}
                                       PROJECT_ID=#{run.project.id}
                                       WEB_HOST=infratester_web
                                       GIT_BRANCH=#{run.branch}
                                       GIT_REPO=#{run.project.repo_url}
                                   ],
                         configs: [report_runner(swarm)]
  end

  def report_runner(swarm, config_name = 'reporter')
    file_hash = { "Name": '/usr/bin/post_results', "UID": '0', "GID": '0', mode: 0755  }
    if config = swarm.config_exists?('reporter')
      { "ConfigID": config['ID'], "ConfigName": config_name, "File": file_hash }
    else
      file_name = File.expand_path('../../templates/post_result.rb.erb', __FILE__)
      template = ERB.new(File.read(file_name)).result(binding)

      report_config = swarm.create_config name: config_name,
                                          filename: '/usr/bin/post_results',
                                          data: Base64.encode64(template)
      report_config[:"File"] = file_hash
      report_config
    end

  end

  def broadcast_update(run)
    data = ApplicationController.render partial: 'runs/run', locals: {run: run}
    ActionCable.server.broadcast("runs_#{run.project.id}", id: run.id.to_s, body: data)
  end

  def update_status(run, status)
    run.last_update = DateTime.now unless run.status == status
    run.status = status
    run.save
    broadcast_update(run)
  end



  def render_report(name, data, level = 0)
    css_class = "#{indentation_css(level)}"
    text = "%a{ id: '#{name}'}\n"
    text += "%div{class: 'test-report'}\n"
    text += "#{indent(1)}%div{class: '#{css_class}'}\n#{indent(2)}#{name}\n"
    data['examples'].each {|k,v| text += render_example_group(k, v, level + 1)}
    text += "#{indent(1)}%div{class: '#{css_class}'}#{data['summary_line']}\n"
    logger.info text
    render_haml text
  end

  def render_example_group(name, data, level = 1)
    css_class = "#{indentation_css(level)}"
    text = "#{indent(level)}%div{class: '#{css_class}'}\n#{indent(level+1)}#{name}\n"
    data.each do |inner_name, inner_data|
      if inner_data.key?('description')
        text += render_example(inner_name, inner_data, level + 1)
      else
        text += render_example_group(inner_name, inner_data, level + 1)
      end
    end
    text
  end

  def render_example(name, data, level)
    css_class = "#{test_result_css(data['status'])} #{indentation_css(level)}"
    "#{indent(level)}%div{class: '#{css_class}'}\n#{indent(level+1)}#{test_result_icon(data['status'])} #{name}\n"
  end



  def suite_status(data)
    logger.info data.to_yaml
    data['summary']['failure_count'].to_i > 0 ? 'failed' : 'passed'
  end

  def render_dashlink(key, data)
    render_haml "%a{href: 'report/#{key}', class: 'btn #{button_css(suite_status(data))}' } #{key}"
  end

  def row_css(status, result)
    if status == 'complete'
      if result == 'passed'
        'run-passed'
      elsif result == 'pending'
        'run-pending'
      elsif result == 'failed'
        'run-failed'
      end
    elsif status == 'failed'
      'run-failed'
    elsif status == 'error'
      'run-error'
    else
      'run-scheduled'
    end
  end

  def result_icon_css(status, result)
    if status == 'complete'
      if result == 'passed'
        'test-passed'
      elsif result == 'pending'
        'test-pending'
      elsif result == 'failed'
        'test-failed'
      end
    elsif status == 'failed'
      'test-failed'
    elsif status == 'error'
      'test-error'
    else
      'test-scheduled'
    end
  end

  def result_icon(status, result)
    if status == 'complete'
      if result == 'passed'
        fa_icon 'check-circle', type: :solid
      elsif result == 'pending'
        fa_icon 'clock', type: :solid
      elsif result == 'failed'
        fa_icon 'times-circle', type: :solid
      end
    elsif status == 'failed'
      fa_icon 'times-circle', type: :solid
    elsif status == 'error'
      fa_icon 'exclamation-circle', type: :solid
    else
      fa_icon 'clock', type: :solid
    end
  end

  def run_summary(run)

  end
end
