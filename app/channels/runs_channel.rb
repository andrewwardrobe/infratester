class RunsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "runs_#{params[:id]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
