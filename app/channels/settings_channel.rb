class SettingsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "settings_#{params[:id]}"
  end
end