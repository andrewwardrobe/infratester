json.extract! run, :id, :status, :result, :created_at, :updated_at
json.url run_url(run, format: :json)
