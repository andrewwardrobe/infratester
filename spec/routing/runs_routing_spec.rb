# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RunsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/projects/1/runs').to route_to('runs#index', project_id: '1')
    end

    it 'routes to #new' do
      expect(get: '/projects/1/runs/new').to route_to('runs#new', project_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/projects/1/runs/1').to route_to('runs#show', id: '1', project_id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/projects/1/runs/1/edit').to route_to('runs#edit', id: '1', project_id: '1')
    end

    it 'routes to #create' do
      expect(post: '/projects/1/runs').to route_to('runs#create', project_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/projects/1/runs/1').to route_to('runs#update', id: '1', project_id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/projects/1/runs/1').to route_to('runs#update', id: '1', project_id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/projects/1/runs/1').to route_to('runs#destroy', id: '1', project_id: '1')
    end
  end
end
