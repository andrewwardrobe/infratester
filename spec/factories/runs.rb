FactoryBot.define do
  factory :run do
    status { "complete" }
    result { "passed" }
    project
  end
end
