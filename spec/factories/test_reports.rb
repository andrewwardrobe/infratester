FactoryBot.define do
  factory :test_report do
    version { "MyString" }
    examples { "" }
    summary { "" }
    summary_line { "MyString" }
  end
end
