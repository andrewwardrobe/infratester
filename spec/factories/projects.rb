FactoryBot.define do
  factory :project do
    name { "Leek" }
    repo_url { "https://gitlab.com/andrewwardrobe/serverspec-launcher-docker.git" }
    factory :project_with_runs do
      transient do
        run_count { 2 }
       end

      after(:create) do |project, evaluator|
        create_list(:run, evaluator.run_count, project: project)
      end
    end
  end
end
