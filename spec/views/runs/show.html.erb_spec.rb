# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'runs/show', type: :view do
  before(:each) do
    project = create(:project)
    @run = assign(:run, create(:run, project: project))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Status: complete/)
    expect(rendered).to match(/Result: passed/)
  end
end
