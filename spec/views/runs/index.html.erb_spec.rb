require 'rails_helper'

RSpec.describe "runs/index", type: :view do
  before(:each) do
    project = create(:project_with_runs)
    @runs = assign(:runs, project.runs)
  end

  it "renders a list of runs" do
    render
    assert_select "tr>td:nth-of-type(2)", :text => "complete".to_s, :count => 2
    assert_select "tr>td:nth-of-type(4)", :html => '<i class="fas fa-check-circle"></i>'.to_s, :count => 2
  end
end
