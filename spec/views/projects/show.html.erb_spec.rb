require 'rails_helper'

RSpec.describe "projects/show", type: :view do
  before(:each) do
    @project = assign(:project, create(:project))
    @runs = @project.runs
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(%r{Leek})
    expect(rendered).to match(%r{https://gitlab.com/andrewwardrobe/serverspec-launcher-docker.git})
  end
end
