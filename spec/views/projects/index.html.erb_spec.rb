require 'rails_helper'

RSpec.describe "projects/index", type: :view do
  before(:each) do
    assign(:projects, create_list(:project, 2))
  end

  it "renders a list of projects" do
    render
    assert_select "tr>td", :text => "Leek".to_s, :count => 2
    assert_select "tr>td", :text => "https://gitlab.com/andrewwardrobe/serverspec-launcher-docker.git".to_s, :count => 2
  end
end
