require 'rails_helper'

RSpec.describe "Runs", type: :request do
  describe "GET /runs" do
    it "works! (now write some real specs)" do
      project = create(:project)
      get project_runs_path project
      expect(response).to have_http_status(200)
    end
  end
end
