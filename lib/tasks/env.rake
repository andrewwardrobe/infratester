require 'yaml'
require 'json'
require 'rspec/core/rake_task'
require_relative '../helpers/rake_helpers'

namespace :environment do
  desc 'bring the environment up'
  task :up do
    run_command 'vagrant up --provision'
  end

  desc 'Destory the environment'
  task :destroy do
    run_command 'vagrant destroy -f'
  end

  desc 'build the environment'
  task :build do
    Rake::Task['environment:up'].invoke
    Rake::Task['ansible:site'].invoke
    Rake::Task['docker:build'].invoke
    Rake::Task['docker:push'].invoke
    File.delete 'tmp/pids/server.pid' if File.exists? 'tmp/pids/server.pid'
    Rake::Task['docker:deploy'].invoke
  end

  desc 'rebuild the environment'
  task :rebuild do
    Rake::Task['environment:destroy'].invoke
    Rake::Task['environment:build'].invoke
  end

  desc 'redeploy the environment'
  task :redeploy do
    Rake::Task['docker:build'].invoke
    Rake::Task['docker:push'].invoke
    File.delete 'tmp/pids/server.pid' if File.exists? 'tmp/pids/server.pid'
    Rake::Task['docker:deploy'].invoke
  end
end
