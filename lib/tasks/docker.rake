require_relative '../helpers/rake_helpers'

def remote_api
  "-H tcp://manager1:2376 --tlsverify --tlscacert ansible/certs/docker/ca.pem --tlscert ansible/certs/docker/#{Socket.gethostname}.pem --tlskey ansible/certs/docker/#{Socket.gethostname}-key.pem"
end

def registry
  "manager1:5000"
end

def app_tags
  tags = ''
  tags << %( -t #{registry}/andrewwardrobe/infratester:#{Rails.configuration.version} )
  tags << %( -t #{registry}/andrewwardrobe/infratester:latest-dev )
  tags << %( -t #{registry}/andrewwardrobe/infratester:latest )
  tags
end

def runner_tags
  tags = ''
  tags << %( -t #{registry}/andrewwardrobe/infratester-runner:#{Rails.configuration.version} )
  tags << %( -t #{registry}/andrewwardrobe/infratester-runner:latest-dev )
  tags << %( -t #{registry}/andrewwardrobe/infratester-runner:latest )
  tags
end


namespace :docker do

  namespace :build do
    desc 'builds the app docker image'
    task :app do
      run_command "docker image build #{app_tags} ."
    end

    desc 'builds the launcher docker image'
    task :runner do
      Dir.chdir 'container' do
        run_command "docker image build #{runner_tags} ."
      end
    end

    desc 'build all container images'
    task :all do
      Rake::Task['docker:build:app'].invoke
      Rake::Task['docker:build:runner'].invoke
    end
  end

  task :build => 'docker:build:all'

  desc 'push the launcher docker image to test registry'
  task :push do
    run_command "docker push #{registry}/andrewwardrobe/infratester"
    run_command "docker push #{registry}/andrewwardrobe/infratester-runner"
  end

  desc 'Deploys the docker stack'
  task :deploy do
    run_command "docker #{remote_api} stack deploy -c docker-compose.yml infratester"
  end

  desc 'Removes the docker stack'
  task :rm do
    run_command "docker #{remote_api} stack rm infratester"
  end

  desc 'Redeploy the stack'
  task :redeploy do
    Rake::Task['docker:rm'].invoke
    File.delete 'tmp/pids/server.pid' if File.exists? 'tmp/pids/server.pid'
    sleep(10)
    Rake::Task['docker:deploy'].invoke
  end
end