# frozen_string_literal: true

Rails.application.routes.draw do
  resources :projects do
    resources :runs
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/projects/:project_id/runs/:id/report(/:suite)', to: 'runs#report', format: :json, as: 'report', defaults: { suite: 'dashboard' }
  post '/projects/:id/refresh', to: 'projects#refresh', as: 'refresh'
  get '/projects/:id(/:branch)/targets', to: 'projects#targets', as: 'targets', defaults: { branch: 'master', format: :json}
  get '/projects/:id(/:branch)/environments', to: 'projects#environments', as: 'environments', defaults: { branch: 'master', format: :json}

  mount ActionCable.server => '/cable'

end
