#!/usr/bin/env sh


git clone ${GIT_REPO} -b ${GIT_BRANCH} /app
cd app
rm -rf reports/*

bundle install
bundle exec rake serverspec


cat /usr/bin/post_results

post_results